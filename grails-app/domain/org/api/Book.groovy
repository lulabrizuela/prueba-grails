package org.api

class Book {
    String name
    static constraints = {
      name blank: false, unique: true
    }
}
