
import org.auth.*

class BootStrap {

    def init = { servletContext ->
      //Normal user
      User user = new User(username: "lula", password: "lula")
      user.save()

      Role roleUser = new Role(authority: "ROLE_USER")
      roleUser.save()
                              
      new UserRole(user: user, role: roleUser).save()

      //Admin user
      User admin = new User(username: "admin", password: "admin")
      admin.save()

      Role roleAdmin = new Role(authority: "ROLE_ADMIN")
      roleAdmin.save()
                              
      new UserRole(user: admin, role: roleAdmin).save()
    }
    def destroy = {
    }
}
